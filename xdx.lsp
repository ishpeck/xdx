; Ishpeck's generic dice roller toy
; File: xdx.lsp
;
;Copyright (c) 2011 Anthony "Ishpeck" Tedjamulia
;
;Permission is hereby granted, free of charge, to any person obtaining a copy
;of this software and associated documentation files (the "Software"), to deal
;in the Software without restriction, including without limitation the rights
;to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
;copies of the Software, and to permit persons to whom the Software is
;furnished to do so, subject to the following conditions:
;
;The above copyright notice and this permission notice shall be included in all
;copies or substantial portions of the Software.
;
;THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
;AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
;OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
;SOFTWARE.
;

(define (doRollSequence times sides) 
    (let ((tms (abs times)) (mml (if (> 0 times) -1 1) ))
         (map (lambda (x) (* mml (+ 1 x)) ) (rand sides tms))))

(define (rollSequence times sides)
     (if (or (< times 1) (< sides 1))
         (list 0)
	 (doRollSequence times sides)))

(define (cleanup strlst) (filter (lambda (x) (not (empty? x))) strlst))
(define (pips str) (cleanup (find-all "[+\\-]*\\d*[d\\d]*" str)))

(define (num x) (or (int x) 1))
(define (rollpip pip) (apply rollSequence (map num (parse pip "d"))))
(define (evalpip pip) (if (find "d" pip) (rollpip pip) (list (int pip))))

(define (shonum n) (if (<= 0 n) (string "+" n) (string n)))
(define (display rolls ttl) 
    (println (date) " ~ "  (trim (join (map shonum rolls)) "+") (string "=" ttl)) ttl)

(define (evaluate str) 
    (let ((tosses (apply append (map evalpip (pips str)))))
        (display tosses (apply + tosses)) ))

(define (consumeInstructions) 
    (while (> (length (read-line) 0))
        (evaluate (current-line)) ))

(context MAIN)
(consumeInstructions)
(exit)
